# tp-ci

Serveur node js pour le cours de CI / CD

### Installation des dépendances

```npm install```

### Lancement du serveur

```npm start```

### Tests unitaires

```npm test```